﻿using System.Collections.Generic;
using System.Web.Http;
using OAuthPractice.ProtectedApi.Models;

namespace OAuthPractice.ProtectedApi.Controllers
{
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        [Authorize]
        [Route("")]
        public List<Order> Get()
        {
            return Order.CreateOrders();
        }

    }

}